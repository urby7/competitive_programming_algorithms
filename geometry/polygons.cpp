/*
 Algorithms on Polygons
 uses point, distance(), ccw(), angle(), vec, pointToVec(), crossProduct
*/

#include <iostream>
#include <vector>
#include <cmath>
#include "basic_geometry.hpp"
using namespace std;

double polygonCircumference(const vector<point>& P) {
    double result = 0.0;

    for (int i = 0; i < P.size() - 1; i++) {
        result += distance(P[i], P[i + 1]);
    }

    return result;
}

double polygonArea(const vector<point>& P) {
    double result = 0.0;

    for (int i = 0; i < P.size()-1; i++) {
        result += (P[i].x * P[i + 1].y - P[i + 1].x * P[i].y);
    }

    return fabs(result) / 2.0;
}

bool isConvex(const vector<point>& P) {  // Check if P is convex.
    if (P.size() <= 3) return false;
    bool isLeft = ccw(P[0], P[1], P[2]);

    for (int i = 1; i < P.size()-1; i++) {
        if (ccw(P[i], P[i + 1], P[(i + 2) == P.size() ? 1 : i + 2]) != isLeft) {
            return false;
        }
    }

    return true;
}

bool inPolygon(point pt, const vector<point> &P) {  // Check if pt is in P (false if on border).
    if (P.size() == 0) return false;
    double sum = 0;

    for (int i = 0; i < P.size() - 1; i++) {
        if (ccw(pt, P[i], P[i + 1])) {
            sum += angle(P[i], pt, P[i + 1]);
        }
        else {
            sum -= angle(P[i], pt, P[i + 1]);
        }
    }

    return (fabs(sum) - 2 * M_PI) == 0;
    //better: fabs(fabs(sum) - 2 * M_PI) < 1e-9;
}

point lineIntersectSeg(point p, point q, point A, point B) {
    double a = B.y - A.y;
    double b = A.x - B.x;
    double c = B.x * A.y - A.x * B.y;
    double u = fabs(a * p.x + b * p.y + c);
    double v = fabs(a * q.x + b * q.y + c);
    return point((p.x * v + q.x * u) / (u+v), (p.y * v + q.y * u) / (u+v)); }


vector<point> cutPolygon(point a, point b, const vector<point> &Q) {  // Cut polygon by line ab.
    vector<point> P;
    for (int i = 0; i < (int)Q.size(); i++) {
        double left1 = crossProduct(pointToVec(a, b), pointToVec(a, Q[i])), left2 = 0;
        if (i != (int)Q.size() - 1) left2 = crossProduct(pointToVec(a, b), pointToVec(a, Q[i + 1]));
        if (left1 > -1e-9) P.push_back(Q[i]);
        if (left1 * left2 < -1e-9) P.push_back(lineIntersectSeg(Q[i], Q[i + 1], a, b));
    }

    if (!P.empty() && !(P.back() == P.front()))
        P.push_back(P.front());

    return P;
}

int main() {
    vector<point> P;  // polygon

    // Sample input.
    int n, x, y;
    cin >> n;

    for (int i = 0; i < n; i++) {
        cin >> x >> y;
        P.push_back(point(x, y));
    }
    P.push_back(P[0]);
    cout << isConvex(P) << endl << polygonArea(P) << endl << polygonCircumference(P) << endl;

    return 0;
}
/*
 6 1 1 3 3 9 1 12 4 9 7 1 7
 non convex, area 49, circ 31.6383
*/
