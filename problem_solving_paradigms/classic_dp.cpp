/*
 Some classical DP problems without string an graph DP

 Displaying the optimal solution can be done in two ways:
 - bottom-up: store the predecesor in every cell in the DP array, construct the solution by jumping to predecesors
 - top-down: recursively choose some element and then compare if the dp function would be equal to the memorised maximum value

 Bottom-up vs top-down:
 - bottom-up: no function call overhead (especially if subproblems are frequently revisited); if dp[i] depends only on dp[i-1], table is not needed (space saving)
 - top-down: not all cells of dp table will necessarily be filled

 Common parameters:
 - array index, 2 indexes of different arrays
 - subarray (index i and j of one array)
 - vertex position
 - knapsack style, something to decrease
 - small set (bitmask)

 Consider offseting all values if dp array indices can be negative.
 Consider recovering one parameter from others if TLE or MLE.
*/

#include <stdio.h>
#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;

// Subarray with max sum in 1D, O(n), can be viewed as DP or greedy.
int max1DSum(int n, int* a) {
    int sum = 0, mx = 0;
    for (int i = 0; i < n; i++) {
        sum += a[i];
        mx = max(mx, sum);
        // Reset to 0 if sum goes below 0.
        if (sum < 0) sum = 0;
    }
    return mx;
}

// Submatrix with max sum in 2D, O(n^4).
int max2Dsum() {
    int n, mx = -1500000, sum = -1500000;
    cin >> n;
    int dp[n][n];

    // Store sum of the matrix from (0, 0) to (i, j) in dp[i][j].
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cin >> dp[i][j];
            if (i > 0) dp[i][j] += dp[i - 1][j];
            if (j > 0) dp[i][j] += dp[i][j - 1];
            if (i > 0 && j > 0) dp[i][j] -= dp[i-1][j-1];  // avoid double counting
        }
    }

    // Try all possibilities, computing sum in O(1) with the help of dp matrix.
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            for (int k = i; k < n; k++) {
                for (int l = j; l < n; l++) {
                    sum = dp[k][l];
                    if (i > 0) sum -= dp[i - 1][l];
                    if (j > 0) sum -= dp[k][j - 1];
                    if (i > 0 && j > 0) sum += dp[i-1][j-1];  // Avoid double subtracting.
                    mx = max(mx, sum);
                }
            }
        }
    }

    return mx;
}

// Longest increasing subsequence, O(n^2).
int LIS(int n, int* a) {
    int dp[n];
    int mx = 0;

    for (int i = 0; i < n; i++) {
        dp[i] = 1;
        // Try all previous elements to determine the current LIS.
        for (int j = 0; j < i; j++) {
            if (a[j] <= a[i]) dp[i] = max(dp[i], dp[j] + 1);
        }
        mx = max(mx, dp[i]);
    }

    return mx;
}

// 0/1 knapsack, O(nW), better recursive since not every cell of dp table will be filled.
int dpbk[1000][1000];
int boundedKnapsack(int n, int* w, int* v, int cap) {
    if (n == -1 || cap == 0) return 0;
    if (dpbk[n][cap] != -1) return dpbk[n][cap];

    // We cannot take this item.
    if (w[n] > cap) dpbk[n][cap] = boundedKnapsack(n-1, w, v, cap);
    // Either take the item or don't.
    else dpbk[n][cap] = max(boundedKnapsack(n-1, w, v, cap), v[n] + boundedKnapsack(n-1, w, v, cap - w[n]));

    return dpbk[n][cap];
}

// Unbounded knapsack, O(nW), better recursive since not every cell of dp table will be filled.
int dpuk[1000];
int unboundedKnapsack(int n, int* w, int* v, int cap) {
    if (cap == 0) return 0;
    if (dpuk[cap] != -1) return dpuk[cap];

    dpuk[cap] = 0;
    // Maximum over every item.
    for (int i = 0; i < n; i++) {
        if (cap >= w[i]) dpuk[cap] = max(dpuk[cap], v[i] + unboundedKnapsack(n, w, v, cap - w[i]));
    }

    return dpuk[cap];
}

// Coin change: what is the minimum number of coins to produce V, O(nV), better recursive since not every cell of dp table will be filled.
int dpc[1000];
int coinChange(int v, int* coins, int n) {
    if (v == 0) return 0;
    if (v < 0) return 500000;
    if (dpc[v] != -1) return dpc[v];

    dpc[v] = 500000;
    // Minimum over every coin.
    for (int i = 0; i < n; i++) {
        dpc[v] = min(dpc[v], 1 + coinChange(v - coins[i], coins, n));
    }

    return dpc[v];
}

// Travelling salesman problem, O(2^n n^2).
int cx[] = {1, 2, 5, 9, 6};
int cy[] = {1, 3, 5, 4, 5};
int dptsp[100][10];
int dist(int i, int j) {
    return abs(cx[i] - cx[j]) + abs(cy[i]- cy[j]);
}

int tsp (int mask, int pos, int n) {
    if (mask == (int)pow(2, n) - 1) return dist(pos, 0);
    if (dptsp[mask][pos] != -1) return dptsp[mask][pos];

    dptsp[mask][pos] = 10000000;
    for (int i = 0; i < n; i++) {
        // If the location is not visited yet, visit it and take minimum.
        if ((mask & (1 << i)) == 0) dptsp[mask][pos] = min(dptsp[mask][pos], dist(pos, i) + tsp(mask | (1 << i), i, n));
    }

    return dptsp[mask][pos];
}

int main() {
    int m1d[] = {4, -5, 4, -3, 4, 4, -4, 4, -5};
    cout << max1DSum(9, m1d) << endl << endl; // 9

    cout << max2Dsum() << endl << endl; // in: 4 0 -2 -7 0 9 2 -6 2 -4 1 -4 1 -1 8 0 -2 out: 15

    int lis[] = {65, 158, 170, 299, 300, 155, 207, 389};
    cout << LIS(8, lis) << endl << endl; // 6

    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 100; j++) {
            dpbk[i][j] = -1;
        }
    }
    int wk[] = {15, 20, 30, 40};
    int vk[] = {120, 100, 60, 200};
    cout << boundedKnapsack(3, wk, vk, 50) << endl << endl; // 220

    memset(dpuk, -1, sizeof dpuk);
    int wk2[] = {1, 60};
    int vk2[] = {1, 70};
    cout << unboundedKnapsack(2, wk2, vk2, 100) << endl << endl; // 110

    memset(dpc, -1, sizeof dpc);
    int coins[] = {1, 3, 4, 5};
    cout << coinChange(7, coins, 4) << endl << endl; // 2

    for (int i = 0; i < 100; i++) {
        for (int j = 0; j < 10; j++) {
            dptsp[i][j] = -1;
        }
    }
    cout << tsp(1, 0, 5) << endl << endl; // 24

    return 0;
}
