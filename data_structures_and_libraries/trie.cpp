/*
 Trie, assuming only lowercase english alphabet
 Build: O(nm)
 Insert: O(m)
 Search: O(m)
 Longest common prefix: O(m)
*/

#include <iostream>
using namespace std;

const int ALPHABET_SIZE = 26;

struct TrieNode {
    TrieNode* children[ALPHABET_SIZE];
    bool endOfWord;  // True if a word ends at the current node
    int children_num;

    TrieNode() {
        for (int i = 0; i < ALPHABET_SIZE; i++)
            children[i] = nullptr;

        endOfWord = false;
        children_num = 0;
    }

    // Warning: copying will not work, be careful or create copy constructor.
    ~TrieNode() {
        for (int i = 0; i < ALPHABET_SIZE; i++) delete children[i];
    }
};

class Trie {
  private:
    TrieNode* root;
    string example;

  public:
    Trie(string *a, int n) {
        root = new TrieNode();
        example = a[0];
        for (int i = 0; i < n; i++)
            insert(a[i]);
    }

    ~Trie() {
        delete root;
    }

    void insert(string key) {
        TrieNode* curr = root;

        // Go to the final index of the key, creating new vertices on the way.
        for (int i = 0; i < key.length(); i++) {
            int index = key[i] - 'a';
            if (curr->children[index] == nullptr) {
                curr->children[index] = new TrieNode();
                curr->children_num++;
            }

            curr = curr->children[index];
        }
        // Mark the end index of key as end of word.
        curr->endOfWord = true;
    }

    bool search(string key) {
        TrieNode* curr = root;

        for (int i = 0; i < key.length(); i++) {
            int index = key[i] - 'a';

            // Next index does not exist, therefore the word does not exist.
            if (!curr->children[index])
                return false;

            curr = curr->children[index];
        }

        return (curr != 0 && curr->endOfWord);
    }

    string longestCommonPrefix() {
        TrieNode* curr = root;
        string res = "";
        for (int i = 0; curr->children_num == 1 && !curr->endOfWord; i++) {
            curr = curr->children[example[i] - 'a'];
            res += example[i];
        }

        return res;
    }

    // Pre-order tree traversal sorts strings in trie in lexicographical order.
    void pre_order(TrieNode* curr, string s) {
        if (curr->endOfWord) cout << s << endl;

        for (int i = 0; i < ALPHABET_SIZE; i++) {
            if (curr->children[i] != 0) {
                pre_order(curr->children[i], s + (char)('a' + i));
            }
        }
    }

    void sortedOut() {
        pre_order(root, "");
    }

    // Delete possible in O(m) but uncommon.
};

int main() {
    string a[] = {"apple", "ape", "april"};
    Trie t(a, 3);

    printf("%d, %d, %d, %d\n", t.search("apple"), t.search("ape"), t.search("applee"), t.search("appl")); // 1, 1, 0, 0
    printf("%s\n\n", t.longestCommonPrefix().c_str()); // ap
    t.sortedOut(); printf("\n"); // ape, apple, april

    string a2[] = {"This", "is", "a", "sample", "piece", "of", "text", "to", "illustrate", "this", "problem"};
    Trie t2(a2, 11);
    t2.sortedOut(); // a illustrate is of piece problem sample text this to
    return 0;
}
