/*
 Binary Indexed Tree or Fenwick Tree
 Build: O(n \log n)
 Update: O(\log n)
 Query: O(\log n)
 */

#include <iostream>
using namespace std;

#define MAX_SIZE 100000

// Both range boundaries inclusive.
class FenwickTree {
  private:
    int tree[MAX_SIZE];
    int n;
  public:
    // Get sum of [0, ... , index].
    int query(int index) {
        index++;  // Fenwick Tree is 1 indexed.
        int sum = 0;

        // While index > 0 and subtract the least significant bit every time.
        for (; index > 0; index -= (index & (-index))) {
            sum += tree[index];
        }

        return sum;
    }

    int query(int i, int j) {
        return query(j) - (i == 0 ? 0 : query(i - 1));
    }

    void update(int index, int diff) {
        index++;  // Fenwick Tree is 1 indexed.

        // While index <= n and increase it by its least significant bit every time.
        for (; index <= n; index += (index & (-index))) {
            tree[index] += diff;
        }
    }

    FenwickTree(int* a, int n) {
        // Fenwick Tree is 1 indexed.
        this->n = n;

        for (int i = 0; i < n + 1; i++) {
            tree[i] = 0;
        }
        for (int i = 0; i < n; i++) {
            update(i, a[i]);
        }
    }
};

int main() {
    // Test 1
    int n = 7;
    int a[] = {1, 2, 3, 4, 5, 6, 7};
    FenwickTree ft1(a, n);

    cout << ft1.query(0, 6) << endl;  // 28
    ft1.update(0, 1);
    cout << ft1.query(0, 6) << endl << ft1.query(1, 6) << endl << endl;  // 29, 27

    // Test 2
    int n2 = 7;
    int a2[] = {18, 17, 13, 19, 15, 11, 20};
    FenwickTree ft2(a2, n2);
    cout << ft2.query(1, 3) << endl << ft2.query(4, 6) << endl;  // 49, 46

    return 0;
}
