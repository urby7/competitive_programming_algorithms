/*
 Disjoint Union/Find Data Structure
 Space: O(n)
 Search/Merge (amortized): O(\alpha n)
 */

#include <iostream>
using namespace std;

#define MAX_SIZE 100000

class UnionFind {
  private:
    int parent[MAX_SIZE];
    int rank[MAX_SIZE];
  public:
    UnionFind(int n) {
        for (int i = 0; i < n; i++) {
            parent[i] = i;
            rank[i] = 0;
        }
    }

    int findSet(int i) {
        if (i != parent[i]) {
            parent[i] = findSet(parent[i]);
        }
        return parent[i];
    }

    bool isSameSet(int i, int j) {
        return findSet(i) == findSet(j);
    }

    void uni(int i, int j) {
        if (!isSameSet(i, j)) {
            int x = findSet(i);
            int y = findSet(j);

            if (rank[x] > rank[y]) {
                parent[y] = x;
            } else {
                parent[x] = y;
                if (rank[x] == rank[y]) {
                    rank[y]++;
                }
            }
        }
    }
};

int main() {
    UnionFind uf(5);
    uf.uni(1, 2);
    cout << uf.isSameSet(1, 2) << endl;  // 1
    cout << uf.isSameSet(2, 3) << endl;  // 0

    return 0;
}
