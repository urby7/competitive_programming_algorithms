/*
 Useful Library Functions
*/

#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
using namespace std;

void stringStream() {  // Included in sstream.
    string s = "one two three\nfour";
    istringstream sin(s);

    string s1;
    while (!sin.eof()) {
        sin >> s1;
        cout << s1 << endl;
    }

    istringstream sin2(s);
    getline(sin2, s1);
    cout << s1 << endl;
}

bool binarySearch(int *a, int n, int val) {  // Binary search in sorted array a.
    if (binary_search(a, a + n, val) == false) return false;

    cout << "Found at range " << lower_bound(a, a + n, val) - a <<
            " - " << upper_bound(a, a + n, val) - 1 - a << endl;
    // or use equal_range()

    return true;
}

void heap() {
    int myInts[] = {10, 20, 30, 5, 15};
    vector<int> v (myInts, myInts + 5);

    if (!is_heap(v.begin(), v.end())) make_heap(v.begin(), v.end());
    cout << v.front() << endl;

    pop_heap(v.begin(), v.end());
    v.pop_back();

    cout << v.front() << endl;

    v.push_back(99);
    push_heap(v.begin(), v.end());
    cout << v.front() << endl;

    sort_heap(v.begin(), v.end());

    for (int i = 0; i < v.size(); i++) {
        cout << v[i] << " ";
    }
    cout << endl;
}

void nthElement(int n) {
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    random_shuffle(a, a+9);
    nth_element(a, a+n, a+9);

    cout << a[n-1] << endl;
}

void containerOperations (vector<int> v1, vector<int> v2) {  // Many more, check reference.
    cout << is_permutation(v1.begin(), v1.end(), v2.begin()) << endl;
    cout << equal(v1.begin(), v1.end(), v2.begin());

    copy(v1.begin(), v1.end(), v2.begin());
    copy_backward(v1.begin(), v1.end(), v2.end());
    fill(v1.begin(), v1.end(), 0);
}

int main() {
    int a[] = {1, 2, 3, 4, 4, 5, 6};
    binarySearch(a, 7, 4);  // Found at range 3 - 4.
    cout << endl;

    heap();  //30
    cout << endl;  //20
                   //99
                   //5 10 15 20 99

    nthElement(8);  //8
    cout << endl;

    return 0;
}
