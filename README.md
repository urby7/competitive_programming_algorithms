# Competitive Programming Algorithms
A collection of algorithms useful for various programming competitions like IOI and ICPC.
Mostly based on [Competitive Programming 3: The New Lower Bound of Programming Contests](https://cpbook.net/) by Felix Halim and Steven Halim. Mistakes are possible, I would appreciate a pull request if you find some.
