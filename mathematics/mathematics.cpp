/*
 Some Mathematical Algorithms
*/

#include <iostream>
using namespace std;

int power/*ByMod*/ (int b, int p /*, int m*/) {
    if (b == 0) return 0;
    if (b == 1) return 1;
    if (p == 0) return 1;

    // b %= m;
    int sum = 1;

    while (p) {
        if (p & 1) {
            sum *= b;
            // sum %= m;
        }
        p >>= 1;
        b *= b;
        // b %= m;
    }

    return sum/*%m*/;
}

int gcd(int a, int b) {
    return b == 0 ? a : gcd(b, a % b);
}

int lcm(int a, int b) {
    return a * (b / gcd(a, b));
}

int f(int x) {
    int ret[] = {6, 6, 0, 1, 4, 3, 3, 4, 0};
    return ret[x];
}

// For finding cycles in functions, also called tortoise and hare, O(cycle_length + offset).
pair<int, int> floydCyleFinding(int x0) {
    // 1st part, finding k*cycle_length, hare's speed is 2 times the tortoise's speed.
    int tortoise = f(x0), hare = f(f(x0));
    while (tortoise != hare) {
        tortoise = f(tortoise);
        hare = f(f(hare));
    }

    //2nd part finding offset, hare and tortoise move at the same speed
    int offset = 0;
    hare = x0;
    while (tortoise != hare) {
        tortoise = f(tortoise);
        hare = f(hare);
        offset++;
    }

    // 3rd part, finding cycle_length, hare moves, tortoise stays.
    int cycle_length = 1;
    hare = f(tortoise);
    while (tortoise != hare) {
        hare = f(hare);
        cycle_length++;
    }

    return pair<int, int>(cycle_length, offset);
}

int main() {
    pair<int, int> ret = floydCyleFinding(2);
    printf("cycle_length (lambda) = %d\noffset (mu) = %d", ret.first, ret.second);  // 3, 2
    return 0;
}
