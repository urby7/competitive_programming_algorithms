/*
 Several Algorithms Regarding Prime Numbers
*/

#include <iostream>
#include <vector>
#include <cmath>
#include <bitset>
using namespace std;

long long sieve_size = 0;
bitset<10000010> bs;
vector<long long> primes;

void sieve(long long upper_bound) {
    sieve_size = upper_bound + 1;
    bs.set();
    bs[0] = 0;
    bs[1] = 0;

    for (long long i = 2; i <= sieve_size; i++) {
        if (bs[i]) {  // It's prime, cross out multiples.
            for (long long j = i * i; j <= sieve_size; j += i) {
                bs[j] = 0;
            }
            primes.push_back(i);
        }
    }
}

bool isPrime(long long n) {  // Important: n <= (last prime in "primes")^2
    if (n <= sieve_size) return bs[n];
    for (const long long& prime : primes) {
        if (n % prime == 0) return false;
    }

    return true;
}

vector<long long int> prime_factors (long long n) {
    vector<long long int> factors;
    long long PF_index = 0, PF = primes[0];

    while (PF * PF <= n) {
        while (n % PF == 0) {
            n /= PF;
            factors.push_back(PF);
        }
        PF = primes[++PF_index];
    }
    if (n != 1) factors.push_back(n);

    return factors;
}

int main() {
    sieve(10000000);  // About the max size.
    cout << isPrime(2147483647) << endl << isPrime(136117223861LL) << endl;  // yes, no

    vector<long long int> r = prime_factors(2147483647);
    for (int i = 0; i < r.size(); i++) cout << r[i] << " ";
    cout << endl;

    r = prime_factors(136117223861LL);  // 104729 1299709
    for (int i = 0; i < r.size(); i++) cout << r[i] << " ";
    cout << endl;

    r = prime_factors(142391208960LL);  // 2 2 2 2 2 2 2 2 2 2 3 3 3 3 5 7 7 7 7 11 13
    for (int i = 0; i < r.size(); i++) cout << r[i] << " ";
    cout << endl;

    return 0;
}
