/*
 Classical Dynamic Programming Problems on Strings
*/

#include <iostream>
using namespace std;

string s1, s2;

// Levenshtein distance, O(mn).
int min_edit_distance(string s1, string s2) {
    int dp[s1.length() + 1][s2.length() + 1];

    for (int i = 0; i < s1.length() + 1; i++) {
        dp[i][0] = i;
    }
    for (int i = 0; i < s2.length() + 1; i++) {
        dp[0][i] = i;
    }

    for (int i = 1; i < s1.length() + 1; i++) {
        for (int j = 1; j < s2.length() + 1; j++) {
            // Delete, insert or swap/do nothing.
            dp[i][j] = min(dp[i - 1][j] + 1,
                           min(dp[i][j - 1] + 1,
                               dp[i - 1][j - 1] + (s1[i - 1] == s2[j - 1] ? 0 : 1)));
        }
    }

    return dp[s1.length()][s2.length()];
}

// O(mn)
int longest_common_subsequence(string s1, string s2) {
    int dp[s1.length() + 1][s2.length() + 1];

    for (int i = 0; i < s1.length() + 1; i++) {
        for (int j = 0; j < s2.length() + 1; j++) {
            dp[i][j] = 0;
        }
    }

    for (int i = 1; i < s1.length() + 1; i++) {
        for (int j = 1; j < s2.length() + 1; j++) {
            // Characters are equal, we increase the longest subsequence by 1.
            if (s1[i - 1] == s2[j - 1]) {
                dp[i][j] = 1 + dp[i - 1][j - 1];
            }
            // Characters are not equal, the longest subsequence stays the same.
            else{
                dp[i][j] = max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
    }

    return dp[s1.length()][s2.length()];
}

// O(n^2)
int dppal[1000][1000];
int longest_palindrome(int l, int r, const string &s) {
    if (dppal[l][r] != -1) return dppal[l][r];
    // Odd length palindrome base case.
    if (l == r) return dppal[l][r] = 1;
    // Even length palindrome base case.
    if (l + 1 == r) return dppal[l][r] = (s[l] == s[r] ? 2 : 1);

    // Continue to the inner word if chars are the same, otherwise take the maximum of both inner words.
    if (s[l] == s[r]) return dppal[l][r] = 2 + longest_palindrome(l + 1, r - 1, s);
    return dppal[l][r] = max(longest_palindrome(l + 1, r, s), longest_palindrome(l, r - 1, s));
}

int main() {
    string s1 = "kitten", s2 = "sitting";
    cout << min_edit_distance(s1, s2) << endl << endl;  // 3
    cout << longest_common_subsequence(s1, s2) << endl << endl;  // 4

    string s3 = "adam";
    for (int i = 0; i < s3.length(); i++) {
        for (int j = 0 ; j < s3.length(); j++) {
            dppal[i][j] = -1;
        }
    }
    cout << longest_palindrome(0, (int)s3.length() - 1, s3) << endl << endl;  // 3

    return 0;
}
