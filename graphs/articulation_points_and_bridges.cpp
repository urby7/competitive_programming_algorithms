/*
 Tarjan's Algortihm for Articulation Points and Bridges (Undirected Graph)
 O(V + E)
*/

#include <iostream>
#include <list>
using namespace std;

#define MAX_SIZE 100000

list<pair<int, int> > adj[MAX_SIZE];  // Adjacency List.
int vis[MAX_SIZE];
int dmin[MAX_SIZE];
int counter = 0;

// vis stores the number of the vertex in the DFS tree.
// min stores the minimum reachable vis vertex through the DFS tree.
void visit(int index, int parent) {
    vis[index] = counter++;
    dmin[index] = vis[index];

    int children = 0;  // For articulation points.

    for (const pair<int, int>& neigh : adj[index]) {
        if (vis[neigh.first] == -1) {
            children++;
            visit(neigh.first, index);
            dmin[index] = min(dmin[index], dmin[neigh.first]);

            // Bridge found, if there is no back edge, the current edge will disconnect the graph.
            if (dmin[neigh.first] > vis[index]) {
                printf("Bridge: %d - %d\n", index, neigh.first);
            }

            // Articulation point found, if there is no back edge, the current vertex will disconnect the graph.
            if (dmin[neigh.first] >= vis[index] && parent != -1) {
                printf("Articulation point: %d\n", index);
                // One articulation point will be found 1 time less than the number of connected subgraphs its removal would produce.
                // To count the number of connected subgraphs the removal of an articulation point produces, count the number of times this condition is true and add 1.
            }
        }
        else if (neigh.first != parent) {
            dmin[index] = min(dmin[index], vis[neigh.first]);
        }
    }

    if (parent == -1 && children > 1) {  // Special case for root as the articulation point.
        printf("Articulation point: %d\n", index);
    }

    return;
}

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    //adj_ is (second vertex, weight)

    for (int i = 0; i < m; i++) {
        cin >> x >> y/* >> w*/;
        adj[x].push_back(make_pair(y, w));
        adj[y].push_back(make_pair(x, w));
    }

    // DFS init.
    for (int i = 0; i < n; i++) {
        vis[i] = -1;
        dmin[i] = -1;
    }

    visit(0, -1);

    return 0;
}
/*
 8 10
 0 1
 0 2
 1 2
 2 3
 3 4
 3 7
 3 5
 7 4
 4 5
 5 6
 Bridges: 5-6, 2-3
 Articulation points: 2, 3, 5
*/
