/*
 Tarjan's Algorithm for Strongly Connected Components (Directed Graph)
 O(E + V)
*/

#include <iostream>
#include <list>
#include <stack>
using namespace std;

#define MAX_SIZE 100000

stack<int> stk;
list<pair<int, int> > adj[MAX_SIZE];  // Adjacency list.
int vis[MAX_SIZE];
int dmin[MAX_SIZE];
bool inStk[MAX_SIZE];
int cnter = 0;

// DFS_visitedSCC stores the number of the vertex in the DFS tree.
// DFS_minSCC stores the number of the minimum reachable DFS_visitedSCC vertex through the DFS tree.
void DFS_visitA(int index) {
    vis[index] = cnter++;
    dmin[index] = vis[index];
    stk.push(index);
    inStk[index] = true;

    for (const pair<int, int>& neigh: adj[index]) {
        if (vis[neigh.first] == -1) {
            DFS_visitA(neigh.first);
            dmin[index] = min(dmin[index], dmin[neigh.first]);
        }
        else if (inStk[neigh.first]) {
            dmin[index] = min(dmin[index], vis[neigh.first]);
        }
    }

    // SCC found, if the current vertex cannot reach back (no back edge), we have come to an end of a SCC group.
    if (vis[index] == dmin[index]) {
        cout << "Strongly connected component: ";
        int i;
        do {
            i = stk.top();
            stk.pop();
            inStk[i] = false;
            cout << i << " ";
        } while (i != index);
        cout << endl;
    }
    return;
}

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    // adj is (second vertex, weight)

    for (int i = 0; i < m; i++) {
        cin >> x >> y/* >> w*/;
        adj[x].push_back(make_pair(y, w));
    }

    //DFS init
    for (int i = 0; i < n; i++) {
        vis[i] = -1;
        dmin[i] = -1;
        inStk[i] = false;
    }

    for (int i = 0; i < n; i++) {
        if (vis[i] == -1) {
            DFS_visitA(i);
        }
    }

    return 0;
}

/*
 8 14
 4 0
 0 1
 1 5
 1 4
 4 5
 1 2
 5 6
 6 5
 2 6
 2 3
 3 2
 6 7
 3 7
 7 7
 Strongly connected: 7; 6,5; 3,2; 4,1,0
*/
