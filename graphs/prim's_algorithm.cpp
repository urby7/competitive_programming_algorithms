/*
 Prim's Algorithm for Minimum Spanning Tree (MST)
 O(E \log V)
*/
#include <iostream>
#include <list>
#include <queue>
using namespace std;

#define MAX_SIZE 100000

list<pair<int, int> > adj[MAX_SIZE];  // Adjacency List
bool taken[MAX_SIZE];
priority_queue<pair<int, int> > pq;

void process(int index) {
    taken[index] = true;
    for (const pair<int, int>& neigh : adj[index]) {
        // Add edge to pq if it can connect a new vertex.
        if (!taken[neigh.first]) {
            pq.push(make_pair(-neigh.second, -neigh.first));  // First sorted by weight, then index, negative to reverse order.
        }
    }
}

int main() {
    // Sample input.
    int n, m, x, y, w = 0;
    cin >> n >> m;
    // adj is (second vertex, weight)
    for (int i = 0; i < m; i++) {
        cin >> x >> y >> w;
        adj[x].push_back(make_pair(y, w));
    }

    // Init.
    for (int i = 0; i < n; i++)
        taken[i] = 0;

    // Algorithm, process first edge.
    process(0);
    int cost = 0, temp_u, temp_w;

    // Add the next edge from pq to the tree if it connects a new vertex.
    while (!pq.empty()) {
        temp_u = -pq.top().second; temp_w = -pq.top().first;
        pq.pop();
        if (!taken[temp_u]) {
            cost += temp_w;
            process(temp_u);
        }
    }

    cout << cost << endl;
    return 0;
}  //Test case: 5 7 0 1 4 0 2 4 0 3 6 0 4 6 1 2 2 2 3 8 3 4 9 Out: 18
